
data "atn-utils_gitlab_package" "zip" {
  count = var.gitlab_package_url == "" ? 0 : 1 
  repository_url = var.gitlab_package_url
  access_token = var.gitlab_api_token
  output_path = var.lambda_file_path
}

