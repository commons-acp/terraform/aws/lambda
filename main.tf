
terraform {
  required_version = ">= 0.13"

  required_providers {
    atn-utils = {
      source = "allence-tunisie/atn-utils"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "4.26.0"
    }
  }
}

