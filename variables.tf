#fondation

variable "lambda_role_arn" {
  description = "Lambda Role execution ARN"
}
#project variable
variable "function_name" {
  description = "Lambda Name"
}
variable "handler" {
  default = "index.handler"
  description = "the lambda handler"
}
variable "runtime" {
  description = "the lambda runtime"
  default = "nodejs16.x"
}
variable "gitlab_package_url" {
  description = "gitlab package URL"
}
variable "gitlab_api_token" {
  description = "gitlab api token"
}
variable "lambda_file_path" {
  description = "lambda file path"
}
variable "lambda_env" {
    type = map(string)
    description = "variable d'environnement utilisé par lambda "
}
