# Outputs LAMBDA
output "lambda_api_arn" {
  value = aws_lambda_function.api.arn
}
output "lambda_api_invoke_arn" {
  value = aws_lambda_alias.api_alias.invoke_arn
}
output "lambda_api_alias" {
  value = aws_lambda_alias.api_alias.name
}

output "lambda_api_function_name" {
  value = aws_lambda_function.api.function_name
}
/*output "aceessTokenstate" {
  value = data.atn-utils_gitlab_package.zip.access_token
}
output "repository_urlState" {
  value = data.atn-utils_gitlab_package.zip.repository_url
}*/
